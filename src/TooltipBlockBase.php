<?php

namespace Drupal\tooltip;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\tooltip\TooltipBlockPluginInterface;

/**
 * Defines a base block implementation that most Tooltip blocks will extend.
 *
 * @ingroup block_api
 */
abstract class TooltipBlockBase extends BlockBase implements TrustedCallbackInterface, TooltipBlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Append arrow.
    $build['#pre_render'][] = [$this, 'setAttributes'];

    // Populate block content.
    $this->tooltip($build);

    // Attach required JS library.
    $build['#attached']['library'][] = 'tooltip/tooltip';

    return $build;
  }

  /**
   * Set custom attributes on block container.
   */
  public function setAttributes($build) {
    // Add wrapper classes around tooltip content.
    // @see Drupal.behavior.tooltip.instanciateTooltip()
    $build['tooltip']['#theme_wrappers'][] = 'container';
    $build['tooltip']['#attributes']['class'][] = 'tooltip';
    $build['tooltip']['#attributes']['class'][] = 'visually-hidden';

    // Cannot set `data-tooltip-id` here because no block UUID available.
    // @see tooltip_block_view_alter()
    // $build['tooltip']['#attributes']['data-toolip-id'] = $block->uuid();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['setAttributes'];
  }

}
